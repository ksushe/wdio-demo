///////////////////////// contactUsTestNPM /////////////////////////////////
var expect = require('chai').expect;
var request = require('sync-request');

browser.addCommand('submitDataViaContactUsForm', function (firstName, lastName, emailAddress, comments) {
    if (firstName) {
        $('[name="first_name"]').setValue(firstName);
    }
    if (lastName) {
        $('[name="last_name"]').setValue(lastName);
    }
    if (emailAddress) {
        $('[name="email"]').setValue(emailAddress);
    }
    if (comments) {
        $('[name="message"]').setValue(comments);
    }
    $('[type="submit"]').click();
});

beforeEach(function () {
    browser.url('/Contact-Us/contactus.html');
});

describe('Test Contact Us form WebdriverUni', function () {
    var res = request('GET', 'http://jsonplaceholder.typicode.com/comments');
    var contactUsDetails = JSON.parse(res.getBody().toString('utf8'));

    beforeEach(function () {
        console.log('Inside the describe block!');
    });

    contactUsDetails.forEach(function (contactDetail) {
        it.only('Should be able to submit a successful submission via contact us form', function (done) {

            //browser.submitDataViaContactUsForm('Joe', 'Blogs', 'joe_blogs@mail.com', 'How much does product x cost?');
            //browser.submitDataViaContactUsForm('Joe', null, 'joe_blogs@mail.com', 'How much does product x cost?');

            browser.submitDataViaContactUsForm('Joe', 'Blogs', contactDetail.email, contactDetail.body);

            var successfulSubmission = $('#contact_reply h1').getText();
            expect(successfulSubmission).to.equal('Thank You for your Message!')
        })
    });
});
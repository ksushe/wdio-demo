describe('IFrame Test', () => {
    beforeEach(function () {
        browser.url('/IFrame/index.html');
        browser.setWindowSize(1200, 800);
        browser.pause(5000);
    })

    it('Test the clicking of a given button housed within a IFrame', () => {
        const iframe = $("#frame");
        browser.switchToFrame(iframe);

        const findOutMore_Button = $("//*[text()='Our Products']"); //create my own selector that will found object by name in DOM  
        findOutMore_Button.waitForDisplayed();
        findOutMore_Button.click();
        browser.pause(5000);

    });
})
////////////// contactUsTestPOM  /////////////////////

import ContactUs_Page from '../pageObjects/ContactUs_Page';

describe('Test contact us page on webdriveruni', () => {
    beforeEach(function () {
        ContactUs_Page.open();
    });

    it('Submit all information via the contact us page', () => {
        ContactUs_Page.successfulContactUsSubmission();
    })

});
module.exports = {
    browser: 'chrome',
    baseUrl: "http://webdriveruniversity.com/Contact-Us/contactus.html",
    windowWidth: 1200,
    windowHeight: 800,
    logLevel: 'info',
    firstName: 'Joe',
    lastName: 'Blog',
    timeout: 15000
}
import Base_PO from './Base_PO'
let config = require('../config/main-config');
let dataGenerators = require('../utils/dataGenerators');

class ContactUs_PO extends Base_PO {
    //Open browser window (export base url from main-config.js file)
    open() {
        super.open(config.baseUrl);
    }

    //Get values ​​by registration field and for the registration confirmation button
    get firstName() {
        return $("//*[@name='first_name']");
    }
    get lastName() {
        return $("//*[@name='last_name']");
    }
    get emailAddress() {
        return $("//*[@name='email']");
    }
    get comments() {
        return $("//*[@name='message']");
    }
    get submitButton() {
        return $("//*[@value='SUBMIT']");
    }

    //Confirmation of registration (click on the registration confirmation button)
    submit() {
        this.submitButton.click();
    }

    //Receiving a successful registration message
    get successfulContactHeader() {
        ///  //div[@id='contact_reply']/h1[.='Thank You for your Message!']   xpath
        return $("//div[@id='contact_reply']/h1");
    }

    //Fill in the registration fields and receive a message about successful registration
    successfulContactUsSubmission() {
        //expect fields to be displayed
        this.firstName.waitForDisplayed(5000);
        //fill in the fields with data (export first name and last name from main-config.js file)
        this.firstName.setValue(config.firstName);
        this.lastName.setValue(config.lastName);
        //fill in the fields with random generate data (export email address and comments from dataGenerators.js)
        this.emailAddress.setValue(dataGenerators.generateRandomEmailAddress());
        this.comments.setValue(dataGenerators.generateRandomString());

        //Confirmation of registration (click on the registration confirmation button)
        this.submit();

        //Receiving a successful registration message 'Thank You for your Message!'
        expect(this.successfulContactHeader.getText()).to.contains('Thank You for your Message!');
    }
}
export default new ContactUs_PO();